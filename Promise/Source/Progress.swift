//
//  Progress.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

import Foundation

public extension Promise {
    @discardableResult
    func progress(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (_ value: Float) -> Void
    ) -> Promise<Value> {
        let promise = reservateProgress(on: queue, group: group, qos: qos, flags: flags, block)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateProgress(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (_ value: Float) -> Void
    ) -> Promise<Value> {
        let promise = Promise<Value>()
        
        observeProgress { state in
            switch state {
            case .dormant:
                break
                
            case .pending(progress: let value):
                queue.async {
                    block(value)
                    
                    promise.updateProgress(value)
                }

            case .fulfilled:
                break
                
            case .rejected:
                break
            }
        }
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break

            case .fulfilled(value: let value):
                promise.fulfill(value)
                
            case .rejected(error: let error):
                promise.reject(error)
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

