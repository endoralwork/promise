//
//  State.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

import Foundation

internal enum State<Value> {
    case dormant
    
    case pending(progress: Float)
    
    case fulfilled(value: Value)
    
    case rejected(error: Error)
}

