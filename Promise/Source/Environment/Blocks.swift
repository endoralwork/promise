//
//  Blocks.swift
//  Promise
//
//  Created by Endoral on 07.01.2022.
//

import Foundation

public extension Promise {
    typealias InitialBlockWithProgress<T> = (_ fulfill: @escaping (T) -> Void, _ reject: @escaping (Error) -> Void, _ progress: @escaping (Float) -> Void) -> Void
    
    typealias InitialBlock<T> = (_ fulfill: @escaping (T) -> Void, _ reject: @escaping (Error) -> Void) -> Void
}

public extension Promise {
    typealias VoidInitialBlockWithProgress = (_ fulfill: @escaping () -> Void, _ reject: @escaping (Error) -> Void, _ progress: @escaping (Float) -> Void) -> Void
    
    typealias VoidInitialBlock = (_ fulfill: @escaping () -> Void, _ reject: @escaping (Error) -> Void) -> Void
}

