//
//  Locker.swift
//  Promise
//
//  Created by Endoral on 04.01.2022.
//

import Foundation
import Dispatch

internal class Locker {
    private let specificKey: DispatchSpecificKey<Bool>

    private let queue: DispatchQueue

    private var isOnQueue: Bool {
        return DispatchQueue.getSpecific(key: specificKey) ?? false
    }
    
    internal init() {
        specificKey = DispatchSpecificKey<Bool>()
        queue = DispatchQueue(label: "com.endoral.promise.locker.dispatchqueue", qos: .userInitiated)
        queue.setSpecific(key: specificKey, value: true)
    }
}

internal extension Locker {
    @discardableResult
    func synchronize<T>(_ action: () -> T) -> T {
        if isOnQueue {
            return action()
        } else {
            return queue.sync(execute: action)
        }
    }
}

