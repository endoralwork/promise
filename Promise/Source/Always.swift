//
//  Always.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

import Foundation

// MARK: Call independent of the result of the completion of the Promise
public extension Promise {
    @discardableResult
    func always(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        let promise = reservateAlways(on: queue, group: group, qos: qos, flags: flags, block)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateAlways(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        let promise = Promise<Value>()
        
        transferProgress(to: promise)
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled(value: let value):
                queue.async(group: group, qos: qos, flags: flags) {
                    do {
                        try block()
                        
                        promise.fulfill(value)
                    } catch {
                        promise.reject(error)
                    }
                }
                
            case .rejected(error: let error):
                queue.async(group: group, qos: qos, flags: flags) {
                    do {
                        try block()
                        
                        promise.reject(error)
                    } catch {
                        promise.reject(error)
                    }
                }
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

