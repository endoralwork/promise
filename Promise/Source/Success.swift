//
//  Success.swift
//  Promise
//
//  Created by Endoral on 08.01.2022.
//

import Foundation

// MARK: Alias for Then to indicate successful completion of the Promise
public extension Promise {
    @discardableResult
    func success(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (Value) throws -> Void
    ) -> Promise<Value> {
        return then(on: queue, group: group, qos: qos, flags: flags, block)
    }
    
    @discardableResult
    func reservateSuccess(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (Value) throws -> Void
    ) -> Promise<Value> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, block)
    }
}

