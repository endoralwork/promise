//
//  Final.swift
//  Promise
//
//  Created by Endoral on 11.04.2022.
//

import Foundation

// MARK: Ending the current Promise chain and creating a new Promise chain with its own progress
public extension Promise {
    @discardableResult
    func final<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: Promise<T>
    ) -> Promise<T> {
        return final(on: queue, group: group, qos: qos, flags: flags, { next })
    }

    @discardableResult
    func final<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlockWithProgress<T>
    ) -> Promise<T> {
        return final(on: queue, group: group, qos: qos, flags: flags, { Promise<T>(block) })
    }

    @discardableResult
    func final<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlock<T>
    ) -> Promise<T> {
        return final(on: queue, group: group, qos: qos, flags: flags, { Promise<T>(block) })
    }
    
    @discardableResult
    func final(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlockWithProgress
    ) -> Promise<Void> {
        return final(on: queue, group: group, qos: qos, flags: flags, { Promise<Void>(block) })
    }

    @discardableResult
    func final(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlock
    ) -> Promise<Void> {
        return final(on: queue, group: group, qos: qos, flags: flags, { Promise<Void>(block) })
    }

    @discardableResult
    func final<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping () throws -> Promise<T>
    ) -> Promise<T> {
        let promise = reservateFinal(on: queue, group: group, qos: qos, flags: flags, next)

        tryStartIfNeeded()

        return promise
    }
    
    @discardableResult
    func reservateFinal<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: Promise<T>
    ) -> Promise<T> {
        return reservateFinal(on: queue, group: group, qos: qos, flags: flags, { next })
    }

    @discardableResult
    func reservateFinal<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlockWithProgress<T>
    ) -> Promise<T> {
        return reservateFinal(on: queue, group: group, qos: qos, flags: flags, { Promise<T>(block) })
    }

    @discardableResult
    func reservateFinal<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlock<T>
    ) -> Promise<T> {
        return reservateFinal(on: queue, group: group, qos: qos, flags: flags, { Promise<T>(block) })
    }
    
    @discardableResult
    func reservateFinal(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlockWithProgress
    ) -> Promise<Void> {
        return reservateFinal(on: queue, group: group, qos: qos, flags: flags, { Promise<Void>(block) })
    }

    @discardableResult
    func reservateFinal(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlock
    ) -> Promise<Void> {
        return reservateFinal(on: queue, group: group, qos: qos, flags: flags, { Promise<Void>(block) })
    }
    
    @discardableResult
    func reservateFinal<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping () throws -> Promise<T>
    ) -> Promise<T> {
        let promise = Promise<T>()
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled:
                fallthrough
                
            case .rejected:
                queue.async(group: group, qos: qos, flags: flags) {
                    do {
                        try next().progress { progress in
                            promise.updateProgress(progress)
                        }.then { nextValue in
                            promise.fulfill(nextValue)
                        }.failure { error in
                            promise.reject(error)
                        }
                    } catch {
                        promise.reject(error)
                    }
                }
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

