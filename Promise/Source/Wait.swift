//
//  After.swift
//  Promise
//
//  Created by Endoral on 07.01.2022.
//

import Foundation

public extension Promise {
    @discardableResult
    func wait(
        time: TimeInterval,
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = []
    ) -> Promise<Value> {
        let promise = reservateWait(time: time, on: queue, group: group, qos: qos, flags: flags)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateWait(
        time: TimeInterval,
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = []
    ) -> Promise<Value> {
        let promise = Promise<Value>()
        
        transferProgress(to: promise)
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled(value: let value):
                queue.asyncAfter(deadline: .now() + time, qos: qos, flags: flags) {
                    promise.fulfill(value)
                }
                
            case .rejected(error: let error):
                queue.asyncAfter(deadline: .now() + time, qos: qos, flags: flags) {
                    promise.reject(error)
                }
            }
        }
        
        transferStart(to: promise)

        return promise
    }
}

