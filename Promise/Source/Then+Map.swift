//
//  Then+Map.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

import Foundation

public extension Promise {
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (Value) throws -> T
    ) -> Promise<T> {
        let promise = reservateThen(on: queue, group: group, qos: qos, flags: flags, block)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (Value) throws -> T
    ) -> Promise<T> {
        let promise = Promise<T>()
        
        transferProgress(to: promise)
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled(value: let value):
                queue.async(group: group, qos: qos, flags: flags) {
                    do {
                        promise.fulfill(try block(value))
                    } catch {
                        promise.reject(error)
                    }
                }
                
            case .rejected(error: let error):
                promise.reject(error)
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

