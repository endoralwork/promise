//
//  Error.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

import Foundation

public extension Promise {
    @discardableResult
    func failure(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (_ error: Error) -> Void
    ) -> Promise<Value> {
        let promise = reservateFailure(on: queue, group: group, qos: qos, flags: flags, block)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateFailure(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping (_ error: Error) -> Void
    ) -> Promise<Value> {
        let promise = Promise<Value>()
        
        transferProgress(to: promise)
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled(value: let value):
                promise.fulfill(value)
                
            case .rejected(error: let error):
                queue.async(group: group, qos: qos, flags: flags) {
                    block(error)
                    
                    promise.reject(error)
                }
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

