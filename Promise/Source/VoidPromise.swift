//
//  VoidPromise.swift
//  Promise
//
//  Created by Endoral on 10.04.2022.
//

import Foundation

public extension Promise {
    typealias VoidPromise = Promise<Void>
}

// MARK: Then
public extension Promise where Value == Void {
    @discardableResult
    func then(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        let promise = reservateThen(on: queue, group: group, qos: qos, flags: flags, block)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateThen(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in try block() })
    }
}

// MARK: Then+Map
public extension Promise where Value == Void {
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> T
    ) -> Promise<T> {
        let promise = reservateThen(on: queue, group: group, qos: qos, flags: flags, block)

        tryStartIfNeeded()

        return promise
    }

    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> T
    ) -> Promise<T> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in try block() })
    }
}

// MARK: Then+Promise
public extension Promise where Value == Void {
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping () throws -> Promise<T>
    ) -> Promise<T> {
        let promise = reservateThen(on: queue, group: group, qos: qos, flags: flags, next)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping () throws -> Promise<T>
    ) -> Promise<T> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in try next() })
    }
}

// MARK: Success
public extension Promise where Value == Void {
    @discardableResult
    func success(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        return then(on: queue, group: group, qos: qos, flags: flags, block)
    }
    
    @discardableResult
    func reservateSuccess(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping () throws -> Void
    ) -> Promise<Value> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, block)
    }
}

