//
//  Promise.swift
//  Promise
//
//  Created by Endoral on 16.12.2021.
//

// MARK: inspired by https://github.com/freshOS/Then

import Foundation

public final class Promise<Value>: AnyPromise {
    private let locker = Locker()
    
    private var _state: State<Value>
    
    private var _isInitialStarted: Bool = false
    
    private var _initialBlock: InitialBlockWithProgress<Value>?
    
    private var _startBlock: (() -> Void)?
    
    private var _progressBlocks: Array<(State<Value>) -> Void>?
    
    private var _blocks: Array<(State<Value>) -> Void>?
    
    public var isStarted: Bool {
        return locker.synchronize {
            switch _state {
            case .dormant:
                return false

            case .pending, .fulfilled, .rejected:
                return true
            }
        }
    }
    
    public var isPending: Bool {
        return locker.synchronize {
            switch _state {
            case .dormant, .fulfilled, .rejected:
                return false

            case .pending:
                return true
            }
        }
    }
    
    public var isFinished: Bool {
        return locker.synchronize {
            switch _state {
            case .dormant, .pending:
                return false
                
            case .fulfilled, .rejected:
                return true
            }
        }
    }
    
    public var value: Value? {
        return locker.synchronize {
            switch _state {
            case .dormant, .pending, .rejected:
                return nil
                
            case .fulfilled(value: let value):
                return value
            }
        }
    }
    
    public var error: Error? {
        return locker.synchronize {
            switch _state {
            case .dormant, .pending, .fulfilled:
                return nil
                
            case .rejected(error: let error):
                return error
            }
        }
    }
    
    @discardableResult
    public convenience init(_ block: @escaping InitialBlockWithProgress<Value>) {
        self.init()

        _initialBlock = { fulfill, reject, progress in
            block(fulfill, reject, progress)
        }
    }
    
    @discardableResult
    public convenience init(_ block: @escaping InitialBlock<Value>) {
        self.init()
        
        _initialBlock = { fulfill, reject, _ in
            block(fulfill, reject)
        }
    }
    
    @discardableResult
    public convenience init(_ block: () -> Value) {
        self.init(block())
    }
    
    @discardableResult
    public convenience init(_ block: () -> Error) {
        self.init(block())
    }
    
    @discardableResult
    public init(_ value: Value) {
        _state = .fulfilled(value: value)
    }
    
    @discardableResult
    public init(_ error: Error) {
        _state = .rejected(error: error)
    }
    
    @discardableResult
    internal init() {
        _state = .dormant
    }
}

public extension Promise where Value == Void {
    @discardableResult
    convenience init(_ block: @escaping VoidInitialBlockWithProgress) {
        self.init()

        _initialBlock = { fulfill, reject, progress in
            block({ fulfill(()) }, reject, progress)
        }
    }
    
    @discardableResult
    convenience init(_ block: @escaping VoidInitialBlock) {
        self.init()
        
        _initialBlock = { fulfill, reject, _ in
            block({ fulfill(()) }, reject)
        }
    }
}

public extension Promise {
    func start() {
        locker.synchronize {
            _start()
        }?()
    }
}

internal extension Promise {
    func tryStartIfNeeded() {
        locker.synchronize {
            [_startInitialIfNeeded(), _start()]
        }.forEach { block in
            block?()
        }
    }
    
    func transferStart<T>(to promise: Promise<T>) {
        let (startBlock, isStarted) = locker.synchronize {
            (self._startBlock ?? self.start, self._isInitialStarted)
        }
        
        promise.locker.synchronize {
            promise._startBlock = startBlock
            promise._isInitialStarted = isStarted
        }
    }
    
    func transferProgress<T>(to promise: Promise<T>) {
        observeProgress { state in
            switch state {
            case .dormant, .fulfilled, .rejected:
                break

            case .pending(progress: let value):
                promise.updateProgress(value)
            }
        }
    }
    
    func observeProgress(_ block: @escaping (State<Value>) -> Void) {
        locker.synchronize {
            switch _state {
            case .dormant, .pending:
                if _progressBlocks == nil {
                    _progressBlocks = [block]
                } else {
                    _progressBlocks?.append(block)
                }
                
                _reportProgress(_state)
                
            case .fulfilled, .rejected:
                block(_state)
            }
        }
    }
    
    func observe(_ block: @escaping (State<Value>) -> Void) {
        locker.synchronize {
            switch _state {
            case .dormant, .pending:
                if _blocks == nil {
                    _blocks = [block]
                } else {
                    _blocks?.append(block)
                }
                
            case .fulfilled, .rejected:
                block(_state)
            }
        }
    }
    
    func updateProgress(_ value: Float) {
        locker.synchronize {
            _reportProgress(_attemptStateAssign(.pending(progress: value)))
        }
    }
    
    func fulfill(_ value: Value) {
        locker.synchronize {
            _report(_attemptStateAssign(.fulfilled(value: value)))
        }
    }

    func reject(_ error: Error) {
        locker.synchronize {
            _report(_attemptStateAssign(.rejected(error: error)))
        }
    }
}
 
private extension Promise {
    func _startInitialIfNeeded() -> (() -> Void)? {
        if _isInitialStarted {
            return nil
        }
        
        _isInitialStarted = true
        
        return _startBlock
    }
    
    func _start() -> (() -> Void)? {
        switch _state {
        case .dormant:
            _ = _attemptStateAssign(.pending(progress: 0))

            guard let initialBlock = _initialBlock else {
                return _reportIfNeeded()
            }

            return {
                self._reportIfNeeded()?()

                initialBlock(self.fulfill, self.reject, self.updateProgress)
            }
            
        case .pending, .fulfilled, .rejected:
            return nil
        }
    }
    
    func _reportIfNeeded() -> (() -> Void)? {
        switch _state {
        case .dormant:
            return nil

        case .pending(progress: let progress):
            if progress != 0 {
                return {
                    self._reportProgress(self._state)
                }
            } else {
                return nil
            }

        case .fulfilled, .rejected:
            return {
                self._report(self._state)
            }
        }
    }
    
    func _reportProgress(_ state: State<Value>) {
        _progressBlocks?.forEach { block in
            block(state)
        }
    }
    
    func _report(_ state: State<Value>) {
        _blocks?.forEach { block in
            block(state)
        }
        
        _initialBlock = nil
        _startBlock = nil
        _progressBlocks = nil
        _blocks = nil
    }
    
    func _attemptStateAssign(_ state: State<Value>) -> State<Value> {
        switch _state {
        case .dormant, .pending:
            _state = state
            
            return state
            
        case .fulfilled, .rejected:
            return _state
        }
    }
}

