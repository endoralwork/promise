//
//  Then+Promise.swift
//  Promise
//
//  Created by Endoral on 22.12.2021.
//

import Foundation
 
public extension Promise {
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: Promise<T>
    ) -> Promise<T> {
        return then(on: queue, group: group, qos: qos, flags: flags, { _ in next })
    }
    
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlockWithProgress<T>
    ) -> Promise<T> {
        return then(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<T>(block) })
    }
    
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlock<T>
    ) -> Promise<T> {
        return then(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<T>(block) })
    }
    
    @discardableResult
    func then(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlockWithProgress
    ) -> Promise<Void> {
        return then(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<Void>(block) })
    }
    
    @discardableResult
    func then(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlock
    ) -> Promise<Void> {
        return then(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<Void>(block) })
    }
    
    @discardableResult
    func then<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping (Value) throws -> Promise<T>
    ) -> Promise<T> {
        let promise = reservateThen(on: queue, group: group, qos: qos, flags: flags, next)
        
        tryStartIfNeeded()
        
        return promise
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: Promise<T>
    ) -> Promise<T> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in next })
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlockWithProgress<T>
    ) -> Promise<T> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<T>(block) })
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping InitialBlock<T>
    ) -> Promise<T> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<T>(block) })
    }
    
    @discardableResult
    func reservateThen(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlockWithProgress
    ) -> Promise<Void> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<Void>(block) })
    }
    
    @discardableResult
    func reservateThen(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ block: @escaping VoidInitialBlock
    ) -> Promise<Void> {
        return reservateThen(on: queue, group: group, qos: qos, flags: flags, { _ in Promise<Void>(block) })
    }
    
    @discardableResult
    func reservateThen<T>(
        on queue: DispatchQueue = .global(),
        group: DispatchGroup? = nil,
        qos: DispatchQoS = .unspecified,
        flags: DispatchWorkItemFlags = [],
        _ next: @escaping (Value) throws -> Promise<T>
    ) -> Promise<T> {
        let promise = Promise<T>()
        
        observe { state in
            switch state {
            case .dormant:
                break
                
            case .pending:
                break
                
            case .fulfilled(value: let value):
                queue.async(group: group, qos: qos, flags: flags) {
                    do {
                        try next(value).progress { progress in
                            promise.updateProgress(progress)
                        }.then { nextValue in
                            promise.fulfill(nextValue)
                        }.failure { error in
                            promise.reject(error)
                        }
                    } catch {
                        promise.reject(error)
                    }
                }
                
            case .rejected(error: let error):
                promise.reject(error)
            }
        }
        
        transferStart(to: promise)
        
        return promise
    }
}

