//
//  Promise.h
//  Promise
//
//  Created by Endoral on 18.01.2022.
//

#import <Foundation/Foundation.h>

// Promise version: 5.0.2

//! Project version number for Promise.
FOUNDATION_EXPORT double PromiseVersionNumber;

//! Project version string for Promise.
FOUNDATION_EXPORT const unsigned char PromiseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Promise/PublicHeader.h>


