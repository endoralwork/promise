// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "Promise",
    products: [
        .library(name: "Promise", type: .static, targets: ["Promise"])
    ],
    targets: [
        .target(
            name: "Promise",
            path: "Promise"
        )
    ]
)

